# Majoo Simply RestfullAPI Test
[![Go Report Card](https://goreportcard.com/badge/github.com/labstack/echo?style=flat-square)](https://gitlab.com/dikhimartin/majoo-test-case/-/pipelines/523355197)

## How to run
``` shell
cp .env.example .env
docker-compose up -d
```

```shell
docker-compose exec go go run .
```
## Server running on
```
http://localhost:4000/api/v1
```

## Credentials info
```
username : admin1
password : admin1

```
```
username : admin2
password : admin2
```

## Documentation
- **Swagger server:**
[http://localhost:8080](http://localhost:8080/)
- **Swagger yaml:**
[swagger.yaml](https://gitlab.com/dikhimartin/majoo-test-case/-/blob/master/docs/swagger.yaml)

## License
Published under the [MIT License](https://github.com/morkid/paginate/blob/master/LICENSE).
