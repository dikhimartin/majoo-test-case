package main

import (
	"api/app/lib"
	"api/app/routes"
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/spf13/viper"
)

// @title Majoo Simply RestfullAPI Test
// @description API Documentation
// @termsOfService https://majoo.id/
// @contact.name Dikhi Martin
// @contact.email dikhi.martin@gmail.com
// @host localhost:4000
// @schemes http
// @BasePath /api/v1

// @securityDefinitions.apikey JWTKeyAuth
// @in header
// @name Authorization
func main() {
	app := fiber.New(fiber.Config{
		Prefork: viper.GetString("PREFORK") == "true",
	})
	routes.Handle(app)
	lib.Logs.Println("Starting Application " + os.Getenv("APP_NAME"))
	log.Fatal(app.Listen(":" + os.Getenv("APP_PORT")))
}
