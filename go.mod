module api

go 1.15

require (
	github.com/aliereno/go-pagination v0.1.0
	github.com/astaxie/beego v1.12.3
	github.com/chilts/sid v0.0.0-20190607042430-660e94789ec9
	github.com/dikhimartin/filters v1.0.4
	github.com/dustin/go-humanize v1.0.0
	github.com/go-playground/validator/v10 v10.10.1
	github.com/go-redis/redis/v8 v8.11.5
	github.com/gofiber/fiber/v2 v2.32.0
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/google/uuid v1.3.0
	github.com/iancoleman/strcase v0.2.0
	github.com/kjk/betterguid v0.0.0-20170621091430-c442874ba63a
	github.com/lithammer/shortuuid v3.0.0+incompatible
	github.com/morkid/gocache v1.0.0
	github.com/morkid/gocache-redis/v8 v8.0.1
	github.com/morkid/paginate v1.1.4
	github.com/oklog/ulid v1.3.1
	github.com/rs/xid v1.4.0
	github.com/segmentio/ksuid v1.0.4
	github.com/sony/sonyflake v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.11.0
	github.com/swaggo/swag v1.8.1
	github.com/valyala/fasthttp v1.35.0
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4
	gorm.io/driver/mysql v1.3.3
	gorm.io/driver/sqlite v1.3.2
	gorm.io/gorm v1.23.4
)
