package middleware

import (
	"api/app/lib"
	"api/app/model"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt"
	"github.com/spf13/viper"
	"time"
)

// ModelJWT model schema
type ModelJWT struct {
	UserID *int `json:"user_id"`
	jwt.StandardClaims
}

// ClaimsJWT func
func ClaimsJWT(c *fiber.Ctx) (jwt.MapClaims, error) {
	accesToken := c.Request().Header.Peek("Authorization")
	token, _, err := new(jwt.Parser).ParseUnverified(string(accesToken), jwt.MapClaims{})
	if err != nil {
		fmt.Println("error parse token : ", err)
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		fmt.Println("Can't convert token's claims to standard claims :", ok)
		return nil, err
	}
	return claims, nil
}

// CreateJwtToken func
func CreateJwtToken(user *model.User) (*string, error) {
	structClaims := ModelJWT{
		user.ID,
		jwt.StandardClaims{
			Id:        lib.IntToStr(*user.ID),
			ExpiresAt: time.Now().Add(viper.GetDuration("SESSION_EXPIRED")).Unix(),
		},
	}
	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS512, structClaims)
	token, err := rawToken.SignedString([]byte("SECRET"))
	if err != nil {
		fmt.Println("Can't createJwtToken:", err)
		return nil, err
	}
	return &token, nil
}

// GetUserID func
func GetUserID(c *fiber.Ctx) (*int, error) {
	dataJwt, err := ClaimsJWT(c)
	if err != nil {
		return nil, err
	}
	res := fmt.Sprintf("%v", dataJwt["user_id"])
	userID := lib.StrToInt(res)
	return &userID, nil
}
