package middleware

import (
	"api/app/config"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// InitEnvirontment func
func InitEnvirontment() {
	env := config.Environment
	// load system env
	systemEnv := viper.New()
	systemEnv.AutomaticEnv()
	for k := range env {
		viper.Set(k, systemEnv.Get(k))
	}

	// load local env
	localEnv := viper.New()
	localEnv.SetConfigType("dotenv")
	localEnv.SetConfigFile(".env")
	if err := localEnv.ReadInConfig(); nil == err {
		localEnvKeys := localEnv.AllKeys()
		for i := range localEnvKeys {
			viper.Set(localEnvKeys[i], localEnv.Get(localEnvKeys[i]))
		}
	}

	// load parameter env
	paramEnv := viper.New()
	paramEnv.AllowEmptyEnv(false)
	for k := range env {
		pflag.String(strings.ReplaceAll(k, "_", "-"), "", k)
	}
	pflag.Parse()
	if err := paramEnv.BindPFlags(pflag.CommandLine); nil == err {
		paramEnvKeys := paramEnv.AllKeys()
		for i := range paramEnvKeys {
			stringValue := paramEnv.GetString(paramEnvKeys[i])
			if stringValue != "" {
				viper.Set(strings.ReplaceAll(paramEnvKeys[i], "-", "_"), stringValue)
			}
		}
	}

	// load default env
	for k, v := range env {
		if !viper.InConfig(k) {
			viper.SetDefault(k, v)
		}
	}

	keys := viper.AllKeys()
	for i := range keys {
		stringValue := viper.GetString(keys[i])
		if stringValue == "" {
			if value := viper.Get(keys[i]); nil != value {
				stringValue = fmt.Sprintf("%v", value)
			}
		}
		os.Setenv(strings.ToUpper(keys[i]), stringValue)
	}
}
