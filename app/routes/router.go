package routes

import (
	"api/app/controller"
	"api/app/controller/auth"
	"api/app/controller/merchant"
	"api/app/controller/outlet"
	"api/app/controller/report"
	"api/app/controller/transaction"
	"api/app/services"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/spf13/viper"
)

// Handle all request to route to controller
func Handle(app *fiber.App) {
	app.Use(cors.New())
	services.InitRedis()
	services.InitDatabase()

	api := app.Group(viper.GetString("ENDPOINT"))
	api.Get("/", controller.ApiIndexGet)
	api.Get("/info.json", controller.ApiInfoGet)

	api.Post("/auth/login", auth.PostLogin)

	// Merchant
	api.Post("/merchants", merchant.PostMerchant)
	api.Get("/merchants", merchant.GetMerchant)
	api.Put("/merchants/:id", merchant.PutMerchant)
	api.Get("/merchants/:id", merchant.GetMerchantID)
	api.Delete("/merchants/:id", merchant.DeleteMerchant)

	// Outlet
	api.Post("/outlets", outlet.PostOutlet)
	api.Get("/outlets", outlet.GetOutlet)
	api.Put("/outlets/:id", outlet.PutOutlet)
	api.Get("/outlets/:id", outlet.GetOutletID)
	api.Delete("/outlets/:id", outlet.DeleteOutlet)

	// Transaction
	api.Get("/transactions", transaction.GetTransaction)
	api.Get("/transactions/:id", transaction.GetTransactionID)

	// Report
	api.Get("/report/merchants/omzet", report.GetReportMerchantOmzet)
	api.Get("/report/outlets/omzet", report.GetReportOutletOmzet)

}
