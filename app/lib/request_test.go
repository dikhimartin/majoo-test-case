package lib

import (
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

// TestBodyParser func
func TestBodyParser(t *testing.T) {
	type sample struct {
		Name *string `validate:"required,gte=9"`
	}

	app := fiber.New()
	app.Post("/validate", func(c *fiber.Ctx) error {
		data := new(sample)
		return ErrorBadRequest(c, BodyParser(c, data))
	})

	res, body, err := PostTest(app, "/validate", nil, "")
	utils.AssertEqual(t, nil, err)
	utils.AssertEqual(t, false, nil == body)
	utils.AssertEqual(t, 400, res.StatusCode)
}
