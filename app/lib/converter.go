package lib

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/dustin/go-humanize"
	"golang.org/x/crypto/bcrypt"
	"strconv"
	"strings"
	"time"
)

// HashPassword func
func HashPassword(password string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes)
}

// CheckPasswordHash func
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// ConvertToMD5 func
func ConvertToMD5(value *int) string {
	str := IntToStr(*value)
	hasher := md5.New()
	hasher.Write([]byte(str))
	converId := hex.EncodeToString(hasher.Sum(nil))
	return converId
}

// ConvertToSHA1 func
func ConvertToSHA1(value string) string {
	sha := sha1.New()
	sha.Write([]byte(value))
	encrypted := sha.Sum(nil)
	encryptedString := fmt.Sprintf("%x", encrypted)
	return encryptedString
}

// ConvertToSHA256 func
func ConvertToSHA256(value string) string {
	hash := sha256.Sum256([]byte(value))
	res := fmt.Sprintf("%x", hash)
	return res
}

// IntToStr func
func IntToStr(value int) string {
	return strconv.Itoa(value)
}

// StrToInt func
func StrToInt(value string) int {
	valueInt, _ := strconv.Atoi(value)
	return valueInt
}

// StrToFloat func
func StrToFloat(value string) float64 {
	valueFloat, _ := strconv.ParseFloat(value, 8)
	return valueFloat
}

// FloatToStr func
func FloatToStr(inputNum float64) string {
	return strconv.FormatFloat(inputNum, 'f', 6, 64)
}

// ConvertJsonToStr func
func ConvertJsonToStr(payload interface{}) string {
	jsonData, err := json.Marshal(payload)
	if err != nil {
		fmt.Println(err)
	}
	return string(jsonData)
}

// ConvertStrToObj func
func ConvertStrToObj(value string) map[string]interface{} {
	var output map[string]interface{}
	err := json.Unmarshal([]byte(value), &output)
	if nil != err {
		// fmt.Println(err)
	}
	return output
}

// ConvertStrToJson func
func ConvertStrToJson(value string) interface{} {
	var output interface{}
	err := json.Unmarshal([]byte(value), &output)
	if nil != err {
		// fmt.Println(err)
	}
	return output
}

// ConvertStrToTime func
func ConvertStrToTime(value string) time.Time {
	layout := "2021-05-19 11:56:30"
	t, err := time.Parse(layout, value)
	if nil != err {
		fmt.Println(err)
	}
	return t
}

// RupiahToFloat func
func RupiahToFloat(value string) float64 {
	s := strings.Replace(value, "Rp ", "", -1)
	t := strings.Replace(s, ",00", "", -1)
	r := strings.Replace(t, ".", "", -1)
	val := strings.Replace(r, "Rp ", "", -1)
	return StrToFloat(val)
}

// RupiahFormat func
func RupiahFormat(value float64) string {
	comma := humanize.Comma(int64(value))
	nominal := strings.Replace(comma, ",", ".", -1)
	return "Rp. " + nominal
}
