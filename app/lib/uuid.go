package lib

import (
	"github.com/chilts/sid"
	guuid "github.com/google/uuid"
	"github.com/kjk/betterguid"
	"github.com/lithammer/shortuuid"
	"github.com/oklog/ulid"
	"github.com/rs/xid"
	"github.com/segmentio/ksuid"
	"github.com/sony/sonyflake"
	"math/rand"
	"time"
	// "github.com/satori/go.uuid"
)

// GenUUIDString func
func GenUUIDString() string {
	id := guuid.New().String()
	return id
}

// StringToUUID func
func StringToUUID(s string) *guuid.UUID {
	res, _ := guuid.Parse(s)
	return &res
}

// GenShortUUID func
func GenShortUUID() string {
	id := shortuuid.New()
	return id
}

// GenUUID func
func GenUUID() *guuid.UUID {
	id := guuid.New()
	return &id
}

// genXid func
func genXid() string {
	id := xid.New()
	return id.String()
}

// genKsuid func
func genKsuid() string {
	id := ksuid.New()
	return id.String()
}

// genBetterGUID func
func genBetterGUID() string {
	id := betterguid.New()
	return id
}

// genUlid func
func genUlid() string {
	t := time.Now().UTC()
	entropy := rand.New(rand.NewSource(t.UnixNano()))
	id := ulid.MustNew(ulid.Timestamp(t), entropy)
	return id.String()
}

// genSonyflake func
func genSonyflake() uint64 {
	flake := sonyflake.NewSonyflake(sonyflake.Settings{})
	id, _ := flake.NextID()
	// Note: this is base16, could shorten by encoding as base62 string
	return id
}

// genSid func
func genSid() string {
	id := sid.Id()
	return id
}
