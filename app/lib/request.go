package lib

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

// VALIDATOR validate request body
var VALIDATOR *validator.Validate = validator.New()

func overrideErrorMessages() {
}

// BodyParser with validation
func BodyParser(c *fiber.Ctx, payload interface{}) error {
	if err := c.BodyParser(payload); nil != err {
		return err
	}
	return VALIDATOR.Struct(payload)
}
