package lib

import (
	"time"
)

// CurrentTime response
func CurrentTime(format string) string {
	if format == "" {
		format = "2006-01-02 15:04:05"
	}
	return time.Now().Format(format)
}

// FormatDate func
func FormatDate(rec, format string) string {
	date, _ := time.Parse("2006-01-02", rec)
	return date.Format(format)
}

// TimeNow response
func TimeNow() *time.Time {
	t := time.Now()
	return &t
}

// CountRangeDate response
func CountRangeDate(date1, date2, types string) float64 {
	// date_1 > date_2
	// format_date : 2006-01-02 15:04:05
	var result float64

	format := "2006-01-02 15:04:05"
	dateOne, _ := time.Parse(format, date1)
	dateTwo, _ := time.Parse(format, date2)

	diff := dateOne.Sub(dateTwo)

	// number of Hours
	if types == "hours" {
		result = diff.Hours()

		// number of Nanoseconds
	} else if types == "nanoseconds" {
		result = float64(diff.Nanoseconds())

		// number of Minutes
	} else if types == "minutes" {
		result = diff.Minutes()

		// number of Seconds
	} else if types == "seconds" {
		result = diff.Seconds()

		// number of Days
	} else if types == "days" {
		result = float64(diff.Hours() / 24)
	}
	return result
}

// RangeTimeAhead response
func RangeTimeAhead(fromDate, format string, years, month, days int) string {
	date, _ := time.Parse("2006-01-02 15:04:05", fromDate)
	t2 := date.AddDate(years, month, days)
	CurrentTimeAhead := t2.Format(format)
	return CurrentTimeAhead
}

// RangeDate returns a date range function over start date to end date inclusive.
// After the end of the range, the range function returns a zero date,
// date.IsZero() is true.
func RangeDate(start, end time.Time) func() time.Time {
	y, m, d := start.Date()
	start = time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
	y, m, d = end.Date()
	end = time.Date(y, m, d, 0, 0, 0, 0, time.UTC)

	return func() time.Time {
		if start.After(end) {
			return time.Time{}
		}
		date := start
		start = start.AddDate(0, 0, 1)
		return date
	}
}
