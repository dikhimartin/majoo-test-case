package auth

import (
	"fmt"

	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"
	"github.com/gofiber/fiber/v2"
)

// PostLogin godoc
// @Summary Request Access Token
// @Description Request Access Token
// @Accept  multipart/form-data
// @Param username formData string true "Username"
// @Param password formData string true "Password"
// @Success 200 {object} model.LoginResponse "Logedin"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Router /auth/login [post]
// @Tags Authentication
func PostLogin(c *fiber.Ctx) error {
	api := new(model.LoginAPI)
	if err := lib.BodyParser(c, api); nil != err {
		return lib.ErrorBadRequest(c, err)
	}

	user, err := CheckAuthentification(api)
	if err != nil {
		return lib.ErrorUnauthorized(c, err.Error())
	}

	accessToken, err := middleware.CreateJwtToken(user)
	if err != nil {
		return lib.ErrorUnauthorized(c, err.Error())
	}

	responseToken := model.ResponseToken{
		AccessToken: accessToken,
	}
	response := model.LoginResponse{
		User:  user,
		Token: &responseToken,
	}

	return lib.OK(c, response)
}

// CheckAuthentification func
func CheckAuthentification(api *model.LoginAPI) (*model.User, error) {
	db := services.DB
	var data model.User
	result := db.Model(&data).First(&data, `user_name = ?`, api.Username)
	if result.RowsAffected < 1 {
		lib.Logs.Println("Failed Loged in cause credentials not found")
		return nil, fmt.Errorf("Failed Loged in cause credentials not found")
	}
	if data.Password != nil {
		validatePassword := lib.CheckPasswordHash(*api.Password, *data.Password)
		if validatePassword != true {
			lib.Logs.Println("Failed Loged in cause credentials not found")
			return nil, fmt.Errorf("Failed Loged in cause credentials not found")
		}
	}
	return &data, nil
}
