package controller

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"api/app/lib"
	"github.com/gofiber/fiber/v2"
)

// SwaggerDocumentationGet func
func SwaggerDocumentationGet(c *fiber.Ctx) error {
	var data interface{}
	file, err := os.Open("docs/swagger.json")
	if err != nil {
		return lib.ErrorNotFound(c, err.Error())
	}
	defer file.Close()

	ReadFile, err := ioutil.ReadAll(file)
	if err != nil {
		return lib.ErrorInternal(c, err.Error())
	}

	err = json.Unmarshal(ReadFile, &data)
	if err != nil {
		return lib.ErrorInternal(c, err.Error())
	}
	return c.JSON(data)
}
