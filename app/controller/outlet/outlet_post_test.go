package outlet

import (
	"api/app/lib"
	"api/app/model"
	"api/app/services"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"github.com/spf13/viper"
)

func TestPostMerchant(t *testing.T) {
	userID := viper.GetInt("USER_ID")

	db := services.DBConnectTest()
	app := fiber.New()

	url := "/outlets"
	app.Post(url, PostOutlet)

	merchant := model.Merchant{
		Base: model.Base{
			ID: lib.Intptr(1),
		},
		DataLog: model.DataLog{
			UpdatedBy: &userID,
			CreatedBy: &userID,
		},
		MerchantAPI: model.MerchantAPI{
			UserID:       &userID,
			MerchantName: lib.Strptr("merchant 1"),
		},
	}
	db.Create(&merchant)

	payload := `{
	  "merchant_id": ` + lib.IntToStr(*merchant.ID) + `,
	  "outlet_name": "Otlet baru"
	}`

	headers := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": viper.GetString("ACCESS_TOKEN"),
	}

	response, body, err := lib.PostTest(app, url, headers, payload)
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 201, response.StatusCode, "getting response code")
	utils.AssertEqual(t, false, nil == body, "validate response body")

	// test invalid json format
	response, _, err = lib.PostTest(app, url, headers, "invalid json format")
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 400, response.StatusCode, "getting response code")

	sqlDB, _ := db.DB()
	sqlDB.Close()
}
