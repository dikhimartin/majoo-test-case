package outlet

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"
	"github.com/dikhimartin/filters"
	"github.com/gofiber/fiber/v2"
)

// GetOutlet godoc
// @Summary List of Outlet
// @Description List of Outlet
// @Param page query int false "Page number start from zero"
// @Param size query int false "Size per page, default `0`"
// @Param sort query string false "Sort by field, adding dash (`-`) at the beginning means descending and vice versa"
// @Param column query string false "custom field `LIKE`, example `['outlet_name']`, change <b>single</b> qoutes to <b>double</b> qoutes"
// @Param search query string false "`Outlet 2`"
// @Param filters query string false "custom filters, example `['id','=','1']`, multiple cases `[['id','=','1'],['AND'],['outlet_name','=','Outlet 1']]`, change <b>single</b> qoutes to <b>double</b> qoutes. symbols <b>('=')</b> is basic operator MYSQL such as `=, !=, IN, NOT IN, LIKE, and etc`"
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} lib.Response{data=model.Page{items=[]model.Outlet}} "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Router /outlets [get]
// @Tags Outlet
func GetOutlet(c *fiber.Ctx) error {
	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}

	// samplePaginate with customQuery
	mod, page, size, total := GetDataOutlet(c, `["merchants.user_id","=","`+lib.IntToStr(*userID)+`"]`, "", "", "", "", 0, 0)
	paginate := filters.GeneratePagination(page, size, total, c.Request().URI().QueryString(), *mod)

	return lib.OK(c, paginate)
}

// GetDataOutlet multiple
func GetDataOutlet(c *fiber.Ctx, customFilters, pageFilters, pageSearch, columnFilter, Sort string, pageNumber, pageSize int) (*[]model.Outlet, int, int, int) {
	db := services.DB

	if pageFilters == "" {
		pageFilters = c.FormValue("filters")
	}
	if pageSearch == "" {
		pageSearch = c.FormValue("search")
	}
	if columnFilter == "" {
		columnFilter = c.FormValue("column")
	}
	if Sort == "" {
		Sort = c.FormValue("sort")
		if Sort == "" {
			Sort = "outlets.id"
		}
	}
	if pageNumber == 0 {
		pageNumber = lib.StrToInt(c.FormValue("page"))
	}
	if pageSize == 0 {
		pageSize = lib.StrToInt(c.FormValue("size"))
		if pageSize == 0 {
			pageSize = 10
		}
	}

	pn := pageNumber
	ps := pageSize
	offset := 0
	if pn >= 0 {
		offset = ps * pn
	}

	var lim int
	if ps == 0 {
		lim = 10
	} else {
		lim = ps
	}

	selected := `outlets.id, 
				 outlets.merchant_id, 
				 merchants.user_id AS user_id, 
				 outlets.outlet_name, 
				 outlets.created_at, 
				 outlets.updated_at, 
				 outlets.created_by, 
				 outlets.updated_by`
	queryFilter, whereFilters, querySearch, whereSearch, orderBy := filters.CreateCustomFilters(pageFilters, pageSearch, columnFilter, Sort)
	queryCustom, whereCustom, _, _, _ := filters.CreateCustomFilters(customFilters, "", "", "")

	results := []model.Outlet{}

	res := db.
		Model(&model.Outlet{}).
		Select(selected).
		Joins("LEFT JOIN merchants ON outlets.merchant_id = merchants.id").
		Where(queryCustom, whereCustom...).
		Where(queryFilter, whereFilters...).
		Where(querySearch, whereSearch...).
		Limit(lim).
		Offset(offset).
		Order(orderBy)
	res.Find(&results)
	totalRecords := filters.CountRecordsData(customFilters, pageFilters, pageSearch, columnFilter, res)
	if res.Error != nil {
		lib.Logs.Println(res.Error)
		return &results, pageNumber, pageSize, totalRecords
	}

	return &results, pageNumber, pageSize, totalRecords
}
