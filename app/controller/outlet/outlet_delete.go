package outlet

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"

	"github.com/gofiber/fiber/v2"
)

// DeleteOutlet godoc
// @Summary Delete Outlet by id
// @Description Delete a Outlet by id
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} lib.Response "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Param id path string true "Outlet ID"
// @Router /outlets/{id} [delete]
// @Tags Outlet
func DeleteOutlet(c *fiber.Ctx) error {
	db := services.DB

	_, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}

	var data model.Outlet
	res := db.Unscoped().Where("id = ?", c.Params("id")).Delete(&data)
	if res.Error != nil {
		return lib.ErrorInternal(c)
	} else if res.RowsAffected == 0 {
		return lib.ErrorNotFound(c)
	}

	return lib.OK(c, nil)
}
