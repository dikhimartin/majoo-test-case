package outlet

import (
	"api/app/controller/merchant"
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"

	"github.com/gofiber/fiber/v2"
)

// PostOutlet godoc
// @Summary Create new Outlet
// @Description Create new Outlet
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 201 {object} model.Outlet "Created"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Param data body model.OutletAPI true "Outlet data"
// @Router /outlets [post]
// @Tags Outlet
func PostOutlet(c *fiber.Ctx) error {
	api := new(model.OutletAPI)
	if err := lib.BodyParser(c, api); nil != err {
		return lib.ErrorBadRequest(c, err)
	}

	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}

	// check merchant are exist
	merchant := merchant.GetDataRowMerchant(`[["id","=","`+lib.IntToStr(*api.MerchantID)+`"],["AND"],["user_id","=","`+lib.IntToStr(*userID)+`"]]`, "", "", "")
	if merchant.ID == nil {
		return lib.ErrorNotFound(c, "merchant are'nt exist on your business")
	}

	db := services.DB
	data := model.Outlet{}
	lib.Merge(api, &data)
	data.CreatedBy = userID
	data.UpdatedBy = userID
	if err := db.Create(&data).Error; nil != err {
		return lib.ErrorInternal(c, err.Error())
	}

	return lib.Created(c, data)
}
