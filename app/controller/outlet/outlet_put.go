package outlet

import (
	"api/app/controller/merchant"
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"

	"github.com/gofiber/fiber/v2"
)

// PutOutlet godoc
// @Summary Update Outlet by id
// @Description Update Outlet by id
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} model.Outlet "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Param id path string true "Outlet ID"
// @Param data body model.OutletAPI true "Outlet data"
// @Router /outlets/{id} [put]
// @Tags Outlet
func PutOutlet(c *fiber.Ctx) error {
	api := new(model.OutletAPI)
	if err := lib.BodyParser(c, api); nil != err {
		return lib.ErrorBadRequest(c, err)
	}

	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}

	// check merchant are exist
	merchant := merchant.GetDataRowMerchant(`[["id","=","`+lib.IntToStr(*api.MerchantID)+`"],["AND"],["user_id","=","`+lib.IntToStr(*userID)+`"]]`, "", "", "")
	if merchant.ID == nil {
		return lib.ErrorNotFound(c, "merchant are'nt exist on your business")
	}

	db := services.DB
	var data model.Outlet
	selected := `outlets.id,  outlets.merchant_id,  merchants.user_id AS user_id,  outlets.outlet_name,  outlets.created_at,  outlets.updated_at,  outlets.created_by,  outlets.updated_by`
	result := db.Model(&data).
		Select(selected).
		Joins("LEFT JOIN merchants ON outlets.merchant_id = merchants.id").
		Where("merchants.user_id = ?", userID).
		First(&data, `outlets.id = ?`, c.Params("id"))
	if result.RowsAffected < 1 {
		return lib.ErrorNotFound(c)
	}

	lib.Merge(api, &data)
	data.UpdatedBy = userID
	if err := db.Updates(&data).Error; nil != err {
		lib.Logs.Println(err)
		return lib.ErrorBadRequest(c, err.Error())
	}

	return lib.OK(c, data)
}
