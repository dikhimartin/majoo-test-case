package outlet

import (
	"api/app/lib"
	"api/app/model"
	"api/app/services"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"github.com/spf13/viper"
)

func TestGetOutlet(t *testing.T) {
	userID := viper.GetInt("USER_ID")
	db := services.DBConnectTest()
	app := fiber.New()
	url := "/outlets"
	app.Get(url, GetOutlet)

	merchant := model.Merchant{
		Base: model.Base{
			ID: lib.Intptr(1),
		},
		DataLog: model.DataLog{
			UpdatedBy: &userID,
			CreatedBy: &userID,
		},
		MerchantAPI: model.MerchantAPI{
			UserID:       &userID,
			MerchantName: lib.Strptr("merchant 1"),
		},
	}
	db.Create(&merchant)

	initial := model.Outlet{
		Base: model.Base{
			ID: lib.Intptr(1),
		},
		OutletAPI: model.OutletAPI{
			MerchantID: merchant.ID,
			OutletName: lib.Strptr("Outlet 1"),
		},
		DataLog: model.DataLog{
			UpdatedBy: &userID,
			CreatedBy: &userID,
		},
	}
	db.Create(&initial)

	uri := url + "?page=0&size=1"

	headers := map[string]string{
		"Authorization": viper.GetString("ACCESS_TOKEN"),
	}

	response, body, err := lib.GetTest(app, uri, headers)
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, response.StatusCode, "getting response code")
	utils.AssertEqual(t, false, nil == body, "validate response body")
	utils.AssertEqual(t, float64(1), body["data"]["visible"], "getting response body")

	sqlDB, _ := db.DB()
	sqlDB.Close()
}
