package outlet

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"github.com/spf13/viper"
)

func TestDeleteOutlet(t *testing.T) {
	middleware.InitEnvirontment()

	db := services.DBConnectTest()
	app := fiber.New()
	app.Delete("/outlets/:id", DeleteOutlet)

	userID := viper.GetInt("USER_ID")
	merchant := model.Merchant{
		Base: model.Base{
			ID: lib.Intptr(1),
		},
		DataLog: model.DataLog{
			UpdatedBy: &userID,
			CreatedBy: &userID,
		},
		MerchantAPI: model.MerchantAPI{
			UserID:       &userID,
			MerchantName: lib.Strptr("merchant 1"),
		},
	}
	db.Create(&merchant)

	initial := model.Outlet{
		Base: model.Base{
			ID: lib.Intptr(1),
		},
		OutletAPI: model.OutletAPI{
			MerchantID: merchant.ID,
			OutletName: lib.Strptr("Outlet 1"),
		},
		DataLog: model.DataLog{
			UpdatedBy: &userID,
			CreatedBy: &userID,
		},
	}
	db.Create(&initial)
	outletID := lib.IntToStr(*initial.ID)

	uri := "/outlets/" + outletID

	headers := map[string]string{
		"Authorization": viper.GetString("ACCESS_TOKEN"),
	}

	response, _, err := lib.DeleteTest(app, uri, headers)
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, response.StatusCode, "getting response code")

	// test delete with non existing id
	response, _, err = lib.DeleteTest(app, uri, headers)
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 404, response.StatusCode, "getting response code")

	sqlDB, _ := db.DB()
	sqlDB.Close()
}
