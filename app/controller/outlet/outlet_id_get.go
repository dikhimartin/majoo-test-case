package outlet

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"
	"github.com/dikhimartin/filters"
	"github.com/gofiber/fiber/v2"
)

// GetOutletID godoc
// @Summary Get a Outlet by id
// @Description Get a Outlet by id
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} lib.Response{data=model.Outlet} "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Param id path string true "Outlet ID"
// @Router /outlets/{id} [get]
// @Tags Outlet
func GetOutletID(c *fiber.Ctx) error {
	db := services.DB

	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}

	var data model.Outlet
	selected := `outlets.id,  outlets.merchant_id,  merchants.user_id AS user_id,  outlets.outlet_name,  outlets.created_at,  outlets.updated_at,  outlets.created_by,  outlets.updated_by`
	result := db.Model(&data).
		Select(selected).
		Joins("LEFT JOIN merchants ON outlets.merchant_id = merchants.id").
		Where("merchants.user_id = ?", userID).
		First(&data, `outlets.id = ?`, c.Params("id"))
	if result.RowsAffected < 1 {
		return lib.ErrorNotFound(c)
	}

	return lib.OK(c, data)
}

// GetDataRowOutlet single
func GetDataRowOutlet(customFilters, pageFilters, pageSearch, columnFilter string) *model.Outlet {
	db := services.DB

	selected := `outlets.id,  outlets.merchant_id,  merchants.user_id AS user_id,  outlets.outlet_name,  outlets.created_at,  outlets.updated_at,  outlets.created_by,  outlets.updated_by`
	queryFilter, whereFilters, querySearch, whereSearch, _ := filters.CreateCustomFilters(pageFilters, pageSearch, columnFilter, "")
	queryCustom, whereCustom, _, _, _ := filters.CreateCustomFilters(customFilters, "", "", "")

	results := model.Outlet{}
	res := db.
		Model(&model.Outlet{}).
		Select(selected).
		Joins("LEFT JOIN merchants ON outlets.merchant_id = merchants.id").
		Where(queryCustom, whereCustom...).
		Where(queryFilter, whereFilters...).
		Where(querySearch, whereSearch...).
		First(&results)
	if res.RowsAffected == 1 {
		return &results
	}
	return &results
}
