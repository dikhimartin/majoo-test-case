package report

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"
	"fmt"
	"github.com/aliereno/go-pagination"
	"github.com/aliereno/go-pagination/frameworks"
	"github.com/aliereno/go-pagination/pages"
	"github.com/dikhimartin/filters"
	"github.com/gofiber/fiber/v2"
	"time"
)

// GetReportMerchantOmzet godoc
// @Summary List of Report merchant omzet
// @Description List of Report merchant omzet </br>
// @Param page query int false "Page number start from zero"
// @Param size query int false "Size per page, default `0`"
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} lib.Response{data=model.Page{items=[]model.ReportMerchantData}} "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Router /report/merchants/omzet [get]
// @Tags Report
func GetReportMerchantOmzet(c *fiber.Ctx) error {
	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}

	each := model.ReportMerchantData{}
	result := []model.ReportMerchantData{}

	format := "2006-01-02"
	start, _ := time.Parse(format, "2021-11-01")
	end := start.AddDate(0, 0, 29)
	no := 0
	for rd := lib.RangeDate(start, end); ; {
		no++
		date := rd()
		if date.IsZero() {
			break
		}
		trxDate := date.Format("2006-01-02")
		trxDateFormat := lib.FormatDate(trxDate, "02 January 2006")
		trx := AccumulateMerchantOmzet(`[["DATE(transactions.created_at)","=","`+trxDate+`"],["AND"],["merchants.user_id","=","`+lib.IntToStr(*userID)+`"]]`, "", "", "")

		each.No = no
		each.Date = &trxDateFormat
		each.MerchantName = trx.MerchantName
		each.Omzet = trx.Omzet
		each.CreatedBy = trx.CreatedBy
		result = append(result, each)
	}

	data := pagination.Paginate(result, pagination.Config{
		PageSize: lib.StrToInt(c.FormValue("size")),
		PageType: pages.LinksPage{},
		Framework: frameworks.Fiber{
			Context: c,
		},
	})
	return lib.OK(c, data)
}

// AccumulateMerchantOmzet formula
func AccumulateMerchantOmzet(customFilters, pageFilters, pageSearch, columnFilter string) *model.RowMerchantOmzetData {
	db := services.DB
	queryFilter, whereFilters, querySearch, whereSearch, _ := filters.CreateCustomFilters(pageFilters, pageSearch, columnFilter, "")
	queryCustom, whereCustom, _, _, _ := filters.CreateCustomFilters(customFilters, "", "", "")

	var id, merchantID, outletID, merchantName, createdBy, omzet []byte
	sum := db.Model(&model.Transaction{}).
		Select("transactions.id, transactions.merchant_id, transactions.outlet_id, merchant_name, users.name, SUM(transactions.bill_total)").
		Joins("LEFT JOIN merchants ON transactions.merchant_id = merchants.id").
		Joins("LEFT JOIN users ON merchants.user_id = users.id").
		Where(queryCustom, whereCustom...).
		Where(queryFilter, whereFilters...).
		Where(querySearch, whereSearch...).
		Row()
	err := sum.Scan(&id, &merchantID, &outletID, &merchantName, &createdBy, &omzet)
	if err != nil {
		fmt.Println("error  :", err)
		lib.Logs.Println(err)
	}
	mN := "no merchant record"
	if string(merchantName) != "" {
		mN = string(merchantName)
	}
	user := "no record"
	if string(createdBy) != "" {
		user = string(createdBy)
	}
	data := model.RowMerchantOmzetData{
		MerchantName: lib.Strptr(mN),
		Omzet:        lib.Float64ptr(lib.StrToFloat(string(omzet))),
		CreatedBy:    lib.Strptr(user),
	}
	return &data
}
