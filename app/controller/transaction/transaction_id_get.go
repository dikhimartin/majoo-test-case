package transaction

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"
	"github.com/dikhimartin/filters"
	"github.com/gofiber/fiber/v2"
)

// GetTransactionID godoc
// @Summary Get a Transaction by id
// @Description Get a Transaction by id
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} lib.Response{data=model.Transaction} "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Param id path string true "Transaction ID"
// @Router /transactions/{id} [get]
// @Tags Transaction
func GetTransactionID(c *fiber.Ctx) error {
	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}
	data := GetDataRowTransaction(`[["transactions.id","=","`+c.Params("id")+`"],["AND"],["merchants.user_id","=","`+lib.IntToStr(*userID)+`"]]`, "", "", "")
	if data.ID == nil {
		return lib.ErrorNotFound(c)
	}
	return lib.OK(c, data)
}

// GetDataRowTransaction single
func GetDataRowTransaction(customFilters, pageFilters, pageSearch, columnFilter string) *model.Transaction {
	db := services.DB

	queryFilter, whereFilters, querySearch, whereSearch, _ := filters.CreateCustomFilters(pageFilters, pageSearch, columnFilter, "")
	queryCustom, whereCustom, _, _, _ := filters.CreateCustomFilters(customFilters, "", "", "")

	selected := `transactions.id, 
				 transactions.merchant_id, 
				 transactions.outlet_id, 
				 transactions.bill_total, 
				 merchants.user_id AS user_id, 
				 transactions.created_at, 
				 transactions.updated_at, 
				 transactions.created_by`
	results := model.Transaction{}
	res := db.
		Model(&model.Transaction{}).
		Select(selected).
		Joins("LEFT JOIN merchants ON transactions.merchant_id = merchants.id").
		Preload("Merchant").
		Preload("Outlet").
		Where(queryCustom, whereCustom...).
		Where(queryFilter, whereFilters...).
		Where(querySearch, whereSearch...).
		First(&results)
	if res.RowsAffected == 1 {
		return &results
	}
	return &results
}
