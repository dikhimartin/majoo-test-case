package transaction

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"github.com/spf13/viper"
)

func TestGetTransaction(t *testing.T) {
	middleware.InitEnvirontment()

	db := services.DBConnectTest()
	app := fiber.New()
	url := "/transactions"
	app.Get(url, GetTransaction)

	userID := viper.GetInt("USER_ID")

	merchant := model.Merchant{
		Base: model.Base{
			ID: lib.Intptr(1),
		},
		DataLog: model.DataLog{
			UpdatedBy: &userID,
			CreatedBy: &userID,
		},
		MerchantAPI: model.MerchantAPI{
			UserID:       &userID,
			MerchantName: lib.Strptr("merchant 1"),
		},
	}
	db.Create(&merchant)

	outlet := model.Outlet{
		Base: model.Base{
			ID: lib.Intptr(1),
		},
		OutletAPI: model.OutletAPI{
			MerchantID: merchant.ID,
			OutletName: lib.Strptr("Outlet 1"),
		},
		DataLog: model.DataLog{
			UpdatedBy: &userID,
			CreatedBy: &userID,
		},
	}
	db.Create(&outlet)

	initial := model.Transaction{
		Base: model.Base{
			ID: lib.Intptr(1),
		},
		TransactionAPI: model.TransactionAPI{
			MerchantID: merchant.ID,
			OutletID:   outlet.ID,
			BillTotal:  lib.Float64ptr(7500),
		},
		DataLog: model.DataLog{
			UpdatedBy: &userID,
			CreatedBy: &userID,
		},
	}
	db.Create(&initial)

	headers := map[string]string{
		"Authorization": viper.GetString("ACCESS_TOKEN"),
	}

	uri := url + "?page=0&size=1"

	response, body, err := lib.GetTest(app, uri, headers)
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, response.StatusCode, "getting response code")
	utils.AssertEqual(t, false, nil == body, "validate response body")
	utils.AssertEqual(t, float64(1), body["data"]["total"], "getting response body")

	sqlDB, _ := db.DB()
	sqlDB.Close()
}
