package transaction

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"
	"github.com/dikhimartin/filters"
	"github.com/gofiber/fiber/v2"
)

// GetTransaction godoc
// @Summary List of Transaction
// @Description List of Transaction </br>
// @description <b>example custom filters :</b></br>
// @description `["DATE(transactions.created_at)","BETWEEN",["2021-11-02 12:30:04","2021-11-03 12:30:04"]]` (YYYY-MM-DD hh:mm:ss)</br>
// @description `["merchant_name","IN",["merchant 1", "merchant 2", "merchant 3"]`</br>
// @Param page query int false "Page number start from zero"
// @Param size query int false "Size per page, default `0`"
// @Param sort query string false "Sort by field, adding dash (`-`) at the beginning means descending and vice versa"
// @Param column query string false "custom field `LIKE`, example `['merchant_name']`, change <b>single</b> qoutes to <b>double</b> qoutes"
// @Param search query string false "`merchant 1`"
// @Param filters query string false "custom filters, example `['DATE(created_at)','BETWEEN',['date_start','date_end']]`"
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} lib.Response{data=model.Page{items=[]model.Transaction}} "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Router /transactions [get]
// @Tags Transaction
func GetTransaction(c *fiber.Ctx) error {
	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}

	// samplePaginate with customQuery
	mod, page, size, total := GetDataTransaction(c, `["merchants.user_id","=","`+lib.IntToStr(*userID)+`"]`, "", "", "", "", 0, 0)
	paginate := filters.GeneratePagination(page, size, total, c.Request().URI().QueryString(), *mod)

	return lib.OK(c, paginate)
}

// GetDataTransaction multiple
func GetDataTransaction(c *fiber.Ctx, customFilters, pageFilters, pageSearch, columnFilter, Sort string, pageNumber, pageSize int) (*[]model.TransactionData, int, int, int) {
	db := services.DB

	if pageFilters == "" {
		pageFilters = c.FormValue("filters")
	}
	if pageSearch == "" {
		pageSearch = c.FormValue("search")
	}
	if columnFilter == "" {
		columnFilter = c.FormValue("column")
	}
	if Sort == "" {
		Sort = c.FormValue("sort")
		if Sort == "" {
			Sort = "transactions.id"
		}
	}
	if pageNumber == 0 {
		pageNumber = lib.StrToInt(c.FormValue("page"))
	}
	if pageSize == 0 {
		pageSize = lib.StrToInt(c.FormValue("size"))
		if pageSize == 0 {
			pageSize = 10
		}
	}

	pn := pageNumber
	ps := pageSize
	offset := 0
	if pn >= 0 {
		offset = ps * pn
	}

	var lim int
	if ps == 0 {
		lim = 10
	} else {
		lim = ps
	}

	selected := `transactions.id, 
				 transactions.merchant_id, 
				 transactions.outlet_id, 
				 transactions.bill_total, 
				 merchants.user_id AS user_id, 
				 merchants.merchant_name AS merchant_name, 
				 outlets.outlet_name AS outlet_name, 
				 transactions.created_at, 
				 transactions.updated_at, 
				 transactions.created_by, 
				 transactions.updated_by`

	queryFilter, whereFilters, querySearch, whereSearch, orderBy := filters.CreateCustomFilters(pageFilters, pageSearch, columnFilter, Sort)
	queryCustom, whereCustom, _, _, _ := filters.CreateCustomFilters(customFilters, "", "", "")

	results := []model.TransactionData{}
	res := db.
		Model(&model.Transaction{}).
		Select(selected).
		Joins("LEFT JOIN merchants ON transactions.merchant_id = merchants.id").
		Joins("LEFT JOIN outlets ON transactions.outlet_id = outlets.id").
		Where(queryCustom, whereCustom...).
		Where(queryFilter, whereFilters...).
		Where(querySearch, whereSearch...).
		Limit(lim).
		Offset(offset).
		Order(orderBy)
	res.Find(&results)
	totalRecords := filters.CountRecordsData(customFilters, pageFilters, pageSearch, columnFilter, res)
	if res.Error != nil {
		lib.Logs.Println(res.Error)
		return &results, pageNumber, pageSize, totalRecords
	}

	return &results, pageNumber, pageSize, totalRecords
}
