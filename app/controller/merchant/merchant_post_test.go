package merchant

import (
	"api/app/lib"
	"api/app/services"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"github.com/spf13/viper"
)

func TestPostMerchant(t *testing.T) {
	db := services.DBConnectTest()
	app := fiber.New()

	url := "/merchants"
	app.Post(url, PostMerchant)

	payload := `{
		"merchant_name": "Toko Majoo Indonesia"
	}`

	headers := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": viper.GetString("ACCESS_TOKEN"),
	}

	response, body, err := lib.PostTest(app, url, headers, payload)
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 201, response.StatusCode, "getting response code")
	utils.AssertEqual(t, false, nil == body, "validate response body")

	// test invalid json format
	response, _, err = lib.PostTest(app, url, headers, "invalid json format")
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 400, response.StatusCode, "getting response code")

	sqlDB, _ := db.DB()
	sqlDB.Close()
}
