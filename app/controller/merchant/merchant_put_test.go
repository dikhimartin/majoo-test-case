package merchant

import (
	"api/app/lib"
	"api/app/model"
	"api/app/services"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"github.com/spf13/viper"
)

func TestPutMerchant(t *testing.T) {
	db := services.DBConnectTest()
	app := fiber.New()

	app.Put("/merchants/:id", PutMerchant)

	userID := viper.GetInt("USER_ID")
	initial := model.Merchant{
		Base: model.Base{
			ID: lib.Intptr(1),
		},
		DataLog: model.DataLog{
			UpdatedBy: &userID,
			CreatedBy: &userID,
		},
		MerchantAPI: model.MerchantAPI{
			UserID:       &userID,
			MerchantName: lib.Strptr("merchant 1"),
		},
	}
	db.Create(&initial)
	merchantID := lib.IntToStr(*initial.ID)

	uri := "/merchants/" + merchantID

	payload := `{
		"merchant_name": "Toko Majoo Indonesia (update)"
	}`

	headers := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": viper.GetString("ACCESS_TOKEN"),
	}

	response, body, err := lib.PutTest(app, uri, headers, payload)
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, response.StatusCode, "getting response code")
	utils.AssertEqual(t, false, nil == body, "validate response body")

	// test invalid json body
	response, _, err = lib.PutTest(app, uri, headers, "invalid json format")
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 400, response.StatusCode, "getting response code")

	// test update with non existing id
	uri = "/merchants/non-existing-id"
	response, _, err = lib.PutTest(app, uri, headers, payload)
	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 404, response.StatusCode, "getting response code")

	sqlDB, _ := db.DB()
	sqlDB.Close()
}
