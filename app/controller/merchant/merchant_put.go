package merchant

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"

	"github.com/gofiber/fiber/v2"
)

// PutMerchant godoc
// @Summary Update Merchant by id
// @Description Update Merchant by id
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} model.Merchant "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Param id path string true "Merchant ID"
// @Param data body model.MerchantAPI true "Merchant data"
// @Router /merchants/{id} [put]
// @Tags Merchant
func PutMerchant(c *fiber.Ctx) error {
	api := new(model.MerchantAPI)
	if err := lib.BodyParser(c, api); nil != err {
		return lib.ErrorBadRequest(c, err)
	}
	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}

	db := services.DB
	var data model.Merchant
	result := db.Model(&data).
		Where(db.Where(model.Merchant{
			MerchantAPI: model.MerchantAPI{
				UserID: userID,
			},
		})).
		First(&data, `id = ?`, c.Params("id"))
	if result.RowsAffected < 1 {
		return lib.ErrorNotFound(c)
	}
	lib.Merge(api, &data)
	data.UpdatedBy = userID
	if err := db.Updates(&data).Error; nil != err {
		lib.Logs.Println(err)
		return lib.ErrorBadRequest(c, err.Error())
	}

	return lib.OK(c, data)
}
