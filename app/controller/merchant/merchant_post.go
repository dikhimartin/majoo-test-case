package merchant

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"

	"github.com/gofiber/fiber/v2"
)

// PostMerchant godoc
// @Summary Create new Merchant
// @Description Create new Merchant
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 201 {object} model.Merchant "Created"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Param data body model.MerchantAPI true "Merchant data"
// @Router /merchants [post]
// @Tags Merchant
func PostMerchant(c *fiber.Ctx) error {
	api := new(model.MerchantAPI)
	if err := lib.BodyParser(c, api); nil != err {
		return lib.ErrorBadRequest(c, err)
	}
	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}
	db := services.DB
	data := model.Merchant{}
	lib.Merge(api, &data)
	data.UserID = userID
	data.CreatedBy = userID
	data.UpdatedBy = userID
	if err := db.Create(&data).Error; nil != err {
		return lib.ErrorInternal(c, err.Error())
	}
	return lib.Created(c, data)
}
