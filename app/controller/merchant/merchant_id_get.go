package merchant

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"
	"github.com/dikhimartin/filters"
	"github.com/gofiber/fiber/v2"
)

// GetMerchantID godoc
// @Summary Get a Merchant by id
// @Description Get a Merchant by id
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} lib.Response{data=model.Merchant} "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Param id path string true "Merchant ID"
// @Router /merchants/{id} [get]
// @Tags Merchant
func GetMerchantID(c *fiber.Ctx) error {
	db := services.DB
	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}
	var data model.Merchant
	result := db.Model(&data).
		Where(db.Where(model.Merchant{
			MerchantAPI: model.MerchantAPI{
				UserID: userID,
			},
		})).
		First(&data, `id = ?`, c.Params("id"))
	if result.RowsAffected < 1 {
		return lib.ErrorNotFound(c)
	}

	return lib.OK(c, data)
}

// GetDataRowMerchant single
func GetDataRowMerchant(customFilters, pageFilters, pageSearch, columnFilter string) *model.Merchant {
	db := services.DB

	queryFilter, whereFilters, querySearch, whereSearch, _ := filters.CreateCustomFilters(pageFilters, pageSearch, columnFilter, "")
	queryCustom, whereCustom, _, _, _ := filters.CreateCustomFilters(customFilters, "", "", "")

	results := model.Merchant{}
	res := db.
		Model(&model.Merchant{}).
		Where(queryCustom, whereCustom...).
		Where(queryFilter, whereFilters...).
		Where(querySearch, whereSearch...).
		First(&results)
	if res.RowsAffected == 1 {
		return &results
	}
	return &results
}
