package merchant

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"

	"github.com/gofiber/fiber/v2"
)

// DeleteMerchant godoc
// @Summary Delete Merchant by id
// @Description Delete a Merchant by id
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} lib.Response "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Param id path string true "Merchant ID"
// @Router /merchants/{id} [delete]
// @Tags Merchant
func DeleteMerchant(c *fiber.Ctx) error {
	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}

	db := services.DB
	var data model.Merchant
	res := db.Unscoped().
		Where(db.Where(model.Merchant{
			MerchantAPI: model.MerchantAPI{
				UserID: userID,
			},
		})).
		Where("id = ?", c.Params("id")).
		Delete(&data)
	if res.Error != nil {
		return lib.ErrorInternal(c)
	} else if res.RowsAffected == 0 {
		return lib.ErrorNotFound(c)
	}

	return lib.OK(c, nil)
}
