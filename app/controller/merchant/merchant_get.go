package merchant

import (
	"api/app/lib"
	"api/app/middleware"
	"api/app/model"
	"api/app/services"
	"github.com/dikhimartin/filters"
	"github.com/gofiber/fiber/v2"
)

// GetMerchant godoc
// @Summary List of Merchant
// @Description List of Merchant
// @Param page query int false "Page number start from zero"
// @Param size query int false "Size per page, default `0`"
// @Param sort query string false "Sort by field, adding dash (`-`) at the beginning means descending and vice versa"
// @Param column query string false "custom field `LIKE`, example `['merchant_name']`, change <b>single</b> qoutes to <b>double</b> qoutes"
// @Param search query string false "`Janji Jiwa`"
// @Param filters query string false "custom filters, example `['user_id','=','1']`, multiple cases `[['user_id','=','1'],['AND'],['merchant_name','=','Merchant 1']]`, change <b>single</b> qoutes to <b>double</b> qoutes. symbols <b>('=')</b> is basic operator MYSQL such as `=, !=, IN, NOT IN, LIKE, and etc`"
// @Accept  application/json
// @Produce  application/json
// @Security JWTKeyAuth
// @Success 200 {object} lib.Response{data=model.Page{items=[]model.Merchant}} "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Router /merchants [get]
// @Tags Merchant
func GetMerchant(c *fiber.Ctx) error {
	userID, err := middleware.GetUserID(c)
	if err != nil {
		return lib.ErrorUnauthorized(c)
	}

	// samplePaginate with customQuery
	mod, page, size, total := GetDataMerchant(c, `["user_id","=","`+lib.IntToStr(*userID)+`"]`, "", "", "", "", 0, 0)
	paginate := filters.GeneratePagination(page, size, total, c.Request().URI().QueryString(), *mod)

	return lib.OK(c, paginate)
}

// GetDataMerchant multiple
func GetDataMerchant(c *fiber.Ctx, customFilters, pageFilters, pageSearch, columnFilter, Sort string, pageNumber, pageSize int) (*[]model.Merchant, int, int, int) {
	db := services.DB

	if pageFilters == "" {
		pageFilters = c.FormValue("filters")
	}
	if pageSearch == "" {
		pageSearch = c.FormValue("search")
	}
	if columnFilter == "" {
		columnFilter = c.FormValue("column")
	}
	if Sort == "" {
		Sort = c.FormValue("sort")
	}
	if pageNumber == 0 {
		pageNumber = lib.StrToInt(c.FormValue("page"))
	}
	if pageSize == 0 {
		pageSize = lib.StrToInt(c.FormValue("size"))
		if pageSize == 0 {
			pageSize = 10
		}
	}

	pn := pageNumber
	ps := pageSize
	offset := 0
	if pn >= 0 {
		offset = ps * pn
	}

	var lim int
	if ps == 0 {
		lim = 10
	} else {
		lim = ps
	}

	queryFilter, whereFilters, querySearch, whereSearch, orderBy := filters.CreateCustomFilters(pageFilters, pageSearch, columnFilter, Sort)
	queryCustom, whereCustom, _, _, _ := filters.CreateCustomFilters(customFilters, "", "", "")
	totalRecords := filters.CountRecordsData(customFilters, pageFilters, pageSearch, columnFilter, db.Model(&model.Merchant{}))

	results := []model.Merchant{}

	res := db.
		Model(&model.Merchant{}).
		Where(queryCustom, whereCustom...).
		Where(queryFilter, whereFilters...).
		Where(querySearch, whereSearch...).
		Limit(lim).
		Offset(offset).
		Order(orderBy).
		Find(&results)
	if res.Error != nil {
		lib.Logs.Println(res.Error)
		return &results, pageNumber, pageSize, totalRecords
	}
	return &results, pageNumber, pageSize, totalRecords
}
