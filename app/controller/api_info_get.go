package controller

import (
	"api/app/lib"

	"github.com/gofiber/fiber/v2"
	"github.com/spf13/viper"
)

// ApiInfoGet func
// @Summary show info response
// @Description show info response
// @Accept  application/json
// @Produce  application/json
// @Success 200 {object} map[string]interface{} "success"
// @Failure 400 {object} lib.Response "bad request"
// @Failure 404 {object} lib.Response "not found"
// @Failure 409 {object} lib.Response "conflict"
// @Failure 500 {object} lib.Response "internal error"
// @Router /info.json [get]
// @Tags Index
func ApiInfoGet(c *fiber.Ctx) error {
	info := fiber.Map{
		"name":         viper.GetString("APP_NAME"),
		"version":      viper.GetString("APP_VERSION"),
		"dependencies": fiber.Map{},
	}
	return lib.OK(c, info)
}
