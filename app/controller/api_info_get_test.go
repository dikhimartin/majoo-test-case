package controller

import (
	"net/http/httptest"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

func TestApiInfoGet(t *testing.T) {
	app := fiber.New()
	app.Get("/info.json", ApiInfoGet)

	response, err := app.Test(httptest.NewRequest("GET", "/info.json", nil))

	utils.AssertEqual(t, nil, err, "GET /info.json")
	utils.AssertEqual(t, 200, response.StatusCode, "HTTP Status")
}
