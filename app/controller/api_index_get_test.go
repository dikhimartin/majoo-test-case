package controller

import (
	"net/http/httptest"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

// TestApiIndexGet test
func TestApiIndexGet(t *testing.T) {
	app := fiber.New()
	app.Get("/", ApiIndexGet)

	response, err := app.Test(httptest.NewRequest("GET", "/", nil))

	utils.AssertEqual(t, nil, err, "GET /")
	utils.AssertEqual(t, 200, response.StatusCode, "HTTP Status")
}
