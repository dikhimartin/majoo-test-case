package config

import "os"

// Environment variables
var Environment map[string]interface{} = map[string]interface{}{
	"app_name":        os.Getenv("APP_NAME"),
	"port":            os.Getenv("APP_PORT"),
	"endpoint":        os.Getenv("ENDPOINT"),
	"environment":     os.Getenv("ENVIRONTMENT"),
	"db_host":         os.Getenv("DB_HOST"),
	"db_port":         os.Getenv("DB_PORT"),
	"db_user":         os.Getenv("DB_USER"),
	"db_pass":         os.Getenv("DB_PASS"),
	"db_name":         os.Getenv("DB_NAME"),
	"db_table_prefix": "",
	"redis_host":      os.Getenv("REDIS_HOST"),
	"redis_port":      os.Getenv("REDIS_PORT"),
	"redis_pass":      "",
	"redis_index":     os.Getenv("REDIS_DB"),
	"prefork":         false,
	"cdn_url_path":    os.Getenv("CDN_URL_PATH"),
	"session_expired": os.Getenv("SESSION_EXPIRED"),
	"user_id":         1,
	"access_token":    "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE2NTA3NzU2MTUsImp0aSI6IjEifQ.eiYfAZrWB2tbsahjviDcABD0hiC1ZPxQHD04ev4XOCe8-gLvqsnIrrcy3JRrGyBqMgudMrs2XBU2Ltg7BSezSA",
}
