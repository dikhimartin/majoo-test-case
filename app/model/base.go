package model

import (
	"api/app/lib"
	"gorm.io/gorm"
)

// Base model
type Base struct {
	ID        *int    `json:"id,omitempty" gorm:"autoIncrement;primaryKey"`                                                                             // model ID
	CreatedAt *string `json:"created_at,omitempty" gorm:"type:timestamp; default:"CURRENT_TIMESTAMP" NOT NULL" format:"date-time" swaggertype:"string"` // created at automatically inserted on post
	UpdatedAt *string `json:"updated_at,omitempty" gorm:"type:timestamp; default:"CURRENT_TIMESTAMP" NOT NULL" format:"date-time" swaggertype:"string"` // updated at automatically changed on put or add on post
}

// DataLog model
type DataLog struct {
	UpdatedBy *int `json:"updated_by,omitempty" gorm:"type:bigint(20); NOT NULL"`
	CreatedBy *int `json:"created_by,omitempty" gorm:"type:bigint(20); NOT NULL"`
}

// BeforeCreate Data
func (b *Base) BeforeCreate(tx *gorm.DB) error {
	now := lib.CurrentTime("")
	if nil == b.CreatedAt {
		b.CreatedAt = &now
	}
	if nil == b.UpdatedAt {
		b.UpdatedAt = &now
	}
	return nil
}

// BeforeUpdate data
func (b *Base) BeforeUpdate(tx *gorm.DB) error {
	now := lib.CurrentTime("")
	b.UpdatedAt = &now
	return nil
}
