package model

import (
	"strconv"
	"strings"
)

// Merchant models *its use to definition table GORM
type Merchant struct {
	Base
	DataLog
	MerchantAPI
}

// TableName *its use to define custom table name
func (Merchant) TableName() string {
	return "merchants"
}

// MerchantAPI Merchant API
type MerchantAPI struct {
	UserID       *int    `json:"user_id,omitempty" gorm:"type:int(40); NOT NULL" swaggerignore:"true"`                                         // User ID
	MerchantName *string `json:"merchant_name,omitempty" example:"Toko Majoo Indonesia" gorm:"type:varchar(40); NOT NULL" validate:"required"` // Merchant Name
}

// Seed data
func (s Merchant) Seed() *[]Merchant {
	data := []Merchant{}
	items := []string{
		"1|1|Merchant 1|1|2",
		"2|2|Merchant 2|1|2",
	}
	for i := range items {
		contents := strings.Split(items[i], "|")
		merchantID, _ := strconv.Atoi(contents[0])
		userID, _ := strconv.Atoi(contents[1])
		var name string = contents[2]
		createdBy, _ := strconv.Atoi(contents[3])
		updatedBy, _ := strconv.Atoi(contents[4])
		data = append(data, Merchant{
			Base: Base{
				ID: &merchantID,
			},
			MerchantAPI: MerchantAPI{
				UserID:       &userID,
				MerchantName: &name,
			},
			DataLog: DataLog{
				CreatedBy: &createdBy,
				UpdatedBy: &updatedBy,
			},
		})
	}

	return &data
}
