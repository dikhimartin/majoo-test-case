package model

import "time"

// LoginAPI model
type LoginAPI struct {
	Username *string `json:"username,omitempty" validate:"required"`
	Password *string `json:"password,omitempty" validate:"required"`
	Remember *bool   `json:"remember,omitempty"`
}

// RegistrationAPI model
type RegistrationAPI struct {
	FirstName      *string `json:"first_name,omitempty"`
	LastName       *string `json:"last_name,omitempty"`
	BusinessName   *string `json:"business_name,omitempty"`
	StoreName      *string `json:"store_name,omitempty"`
	Email          *string `json:"email,omitempty"`
	ProvinceID     *string `json:"province_id,omitempty"`
	CityID         *string `json:"city_id,omitempty"`
	SubdistrictID  *string `json:"subdistrict_id,omitempty"`
	ZipCode        *string `json:"zip_code,omitempty"`
	Username       *string `json:"username,omitempty"`
	Password       *string `json:"password,omitempty"`
	VerifyPassword *string `json:"verify_password,omitempty"`
	ReferralCode   *string `json:"referral_code,omitempty"`
}

// LoginResponse model
type LoginResponse struct {
	Token *ResponseToken `json:"token,omitempty"`
	User  *User          `json:"user,omitempty"`
}

// RegistrationResponse model
type RegistrationResponse struct {
	User *User `json:"user,omitempty"`
}

// ResponseToken model
type ResponseToken struct {
	AccessToken  *string `json:"access_token,omitempty"`
	ExpiresIn    *int    `json:"expires_in,omitempty"`
	RefreshToken *string `json:"refresh_token,omitempty"`
	Scope        *string `json:"scope,omitempty"`
	TokenType    *string `json:"token_type,omitempty"`
}

// ResponseAuthenticate models
type ResponseAuthenticate struct {
	ClientID            *string    `json:"ClientID,omitempty"`
	UserID              *string    `json:"UserID,omitempty"`
	RedirectURI         *string    `json:"RedirectURI,omitempty"`
	Scope               *string    `json:"Scope,omitempty"`
	Code                *string    `json:"Code,omitempty"`
	CodeChallenge       *string    `json:"CodeChallenge,omitempty"`
	CodeChallengeMethod *string    `json:"CodeChallengeMethod,omitempty"`
	CodeCreateAt        *time.Time `json:"CodeCreateAt,omitempty"`
	CodeExpiresIn       *int       `json:"CodeExpiresIn,omitempty"`
	Access              *string    `json:"Access,omitempty"`
	AccessCreateAt      *time.Time `json:"AccessCreateAt,omitempty"`
	AccessExpiresIn     *int64     `json:"AccessExpiresIn,omitempty"`
	Refresh             *string    `json:"Refresh,omitempty"`
	RefreshCreateAt     *time.Time `json:"RefreshCreateAt,omitempty"`
	RefreshExpiresIn    *int64     `json:"RefreshExpiresIn,omitempty"`
}
