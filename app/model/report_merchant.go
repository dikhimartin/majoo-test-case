package model

// ReportMerchantData model data
type ReportMerchantData struct {
	No           int      `json:"no"`
	MerchantName *string  `json:"merchant_name"`
	Date         *string  `json:"date"`
	Omzet        *float64 `json:"omzet"`
	CreatedBy    *string  `json:"created_by"`
}

// RowMerchantOmzetData model data
type RowMerchantOmzetData struct {
	MerchantID   *int     `json:"merchant_id"`
	MerchantName *string  `json:"merchant_name"`
	OutletID     *int     `json:"outlet_id"`
	Omzet        *float64 `json:"omzet"`
	CreatedBy    *string  `json:"created_by"`
}
