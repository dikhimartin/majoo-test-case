package model

import (
	"api/app/lib"
	"strconv"
	"strings"
)

// Transaction models *its use to definition table GORM
type Transaction struct {
	Base
	DataLog
	TransactionAPI
	Merchant *Merchant `json:"merchant,omitempty" gorm:"foreignKey:MerchantID;references:ID"`
	Outlet   *Outlet   `json:"outlet,omitempty" gorm:"foreignKey:OutletID;references:ID"`
}

// TableName *its use to define custom table name
func (Transaction) TableName() string {
	return "transactions"
}

// TransactionAPI Transaction API
type TransactionAPI struct {
	MerchantID *int     `json:"merchant_id,omitempty" gorm:"type:bigint(20); NOT NULL"` // Merchant ID
	OutletID   *int     `json:"outlet_id,omitempty" gorm:"type:bigint(20); NOT NULL"`   // Outlet ID
	BillTotal  *float64 `json:"bill_total,omitempty" gorm:"double; NOT NULL"`           // Bill Total
}

// TransactionData list
type TransactionData struct {
	Transaction
	MerchantName *string `json:"merchant_name,omitempty"`
	OutletName   *string `json:"outlet_name,omitempty"`
}

// Seed data
func (s Transaction) Seed() *[]Transaction {
	data := []Transaction{}
	items := []string{
		"1|1|1|2000|2021-11-01 12:30:04|1|2021-11-01 12:30:04|1",
		"2|2|1|2500|2021-11-01 17:20:14|1|2021-11-01 17:20:14|1",
		"3|1|1|4000|2021-11-02 12:30:04|1|2021-11-02 12:30:04|1",
		"4|1|1|1000|2021-11-04 12:30:04|1|2021-11-04 12:30:04|1",
		"5|1|1|7000|2021-11-05 16:59:30|1|2021-11-05 16:59:30|1",
		"6|1|3|2000|2021-11-02 18:30:04|1|2021-11-02 18:30:04|1",
		"7|1|3|2500|2021-11-03 17:20:14|1|2021-11-03 17:20:14|1",
		"8|1|3|4000|2021-11-04 12:30:04|1|2021-11-04 12:30:04|1",
		"9|1|3|1000|2021-11-04 12:31:04|1|2021-11-04 12:31:04|1",
		"10|1|3|7000|2021-11-05 16:59:30|1|2021-11-05 16:59:30|1",
		"11|2|2|2000|2021-11-01 18:30:04|2|2021-11-01 18:30:04|2",
		"12|2|2|2500|2021-11-02 17:20:14|2|2021-11-02 17:20:14|2",
		"13|2|2|4000|2021-11-03 12:30:04|2|2021-11-03 12:30:04|2",
		"14|2|2|1000|2021-11-04 12:31:04|2|2021-11-04 12:31:04|2",
		"15|2|2|7000|2021-11-05 16:59:30|2|2021-11-05 16:59:30|2",
		"16|2|2|2000|2021-11-05 18:30:04|2|2021-11-05 18:30:04|2",
		"17|2|2|2500|2021-11-06 17:20:14|2|2021-11-06 17:20:14|2",
		"18|2|2|4000|2021-11-07 12:30:04|2|2021-11-07 12:30:04|2",
		"19|2|2|1000|2021-11-08 12:31:04|2|2021-11-08 12:31:04|2",
		"20|2|2|7000|2021-11-09 16:59:30|2|2021-11-09 16:59:30|2",
		"21|2|2|1000|2021-11-10 12:31:04|2|2021-11-10 12:31:04|2",
		"22|2|2|7000|2021-11-11 16:59:30|2|2021-11-11 16:59:30|2",
	}
	for i := range items {
		contents := strings.Split(items[i], "|")

		trxID, _ := strconv.Atoi(contents[0])
		merchantID, _ := strconv.Atoi(contents[1])
		outletID, _ := strconv.Atoi(contents[2])
		billTotal := lib.StrToFloat(contents[3])
		createdAt := contents[4]
		createdBy, _ := strconv.Atoi(contents[5])
		updatedAt := contents[6]
		updatedBy, _ := strconv.Atoi(contents[7])
		data = append(data, Transaction{
			Base: Base{
				ID:        &trxID,
				CreatedAt: &createdAt,
				UpdatedAt: &updatedAt,
			},
			TransactionAPI: TransactionAPI{
				MerchantID: &merchantID,
				OutletID:   &outletID,
				BillTotal:  &billTotal,
			},
			DataLog: DataLog{
				CreatedBy: &createdBy,
				UpdatedBy: &updatedBy,
			},
		})
	}
	return &data
}
