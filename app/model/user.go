package model

import (
	"api/app/lib"
	"strconv"
	"strings"
)

// User models *its use to definition table GORM
type User struct {
	Base
	DataLog
	UserAPI
}

// TableName *its use to define custom table name
func (User) TableName() string {
	return "users"
}

// UserAPI User API
type UserAPI struct {
	Name     *string `json:"name,omitempty" example:"Pengguna" gorm:"type:varchar(45)"`      // Name
	UserName *string `json:"user_name,omitempty" example:"username" gorm:"type:varchar(45)"` // User Name
	Password *string `json:"password,omitempty" gorm:"type:varchar(255)"`                    // Password
}

// Seed data
func (s User) Seed() *[]User {
	data := []User{}
	items := []string{
		"1|Admin 1|admin1|admin1",
		"2|Admin 2|admin2|admin2",
	}
	for i := range items {
		contents := strings.Split(items[i], "|")
		userID, _ := strconv.Atoi(contents[0])
		var name string = contents[1]
		var userName string = contents[2]
		var password string = lib.HashPassword(contents[3])
		data = append(data, User{
			Base: Base{
				ID: &userID,
			},
			UserAPI: UserAPI{
				Name:     &name,
				UserName: &userName,
				Password: &password,
			},
			DataLog: DataLog{
				CreatedBy: &userID,
				UpdatedBy: &userID,
			},
		})
	}

	return &data
}
