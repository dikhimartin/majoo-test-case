package model

import (
	"strconv"
	"strings"
)

// Outlet models *its use to definition table GORM
type Outlet struct {
	Base
	DataLog
	OutletAPI
}

// TableName *its use to define custom table name
func (Outlet) TableName() string {
	return "outlets"
}

// OutletAPI Outlet API
type OutletAPI struct {
	MerchantID *int    `json:"merchant_id,omitempty" gorm:"type:bigint(20); NOT NULL" validate:"required"`  // Merchant ID
	OutletName *string `json:"outlet_name,omitempty" gorm:"type:varchar(40); NOT NULL" validate:"required"` // Outlet Name
}

// Seed data
func (s Outlet) Seed() *[]Outlet {
	data := []Outlet{}
	items := []string{
		"1|1|Outlet 1|1|1",
		"2|2|Outlet 1|2|2",
		"3|1|Outlet 2|1|1",
	}
	for i := range items {
		contents := strings.Split(items[i], "|")
		outletID, _ := strconv.Atoi(contents[0])
		merchantID, _ := strconv.Atoi(contents[1])
		var name string = contents[2]
		createdBy, _ := strconv.Atoi(contents[3])
		updatedBy, _ := strconv.Atoi(contents[4])
		data = append(data, Outlet{
			Base: Base{
				ID: &outletID,
			},
			OutletAPI: OutletAPI{
				MerchantID: &merchantID,
				OutletName: &name,
			},
			DataLog: DataLog{
				CreatedBy: &createdBy,
				UpdatedBy: &updatedBy,
			},
		})
	}
	return &data
}
