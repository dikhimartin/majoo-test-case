package model

// ReportOutletData model data
type ReportOutletData struct {
	No           int      `json:"no"`
	MerchantName *string  `json:"merchant_name"`
	OutletName   *string  `json:"outlet_name"`
	Date         *string  `json:"date"`
	Omzet        *float64 `json:"omzet"`
	CreatedBy    *string  `json:"created_by"`
}

// RowOutletOmzetData model data
type RowOutletOmzetData struct {
	MerchantID   *int     `json:"merchant_id"`
	MerchantName *string  `json:"merchant_name"`
	OutletID     *int     `json:"outlet_id"`
	OutletName   *string  `json:"outlet_name"`
	Omzet        *float64 `json:"omzet"`
	CreatedBy    *string  `json:"created_by"`
}
