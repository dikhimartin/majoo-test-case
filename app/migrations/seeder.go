package migrations

import "api/app/model"

var (
	user        model.User
	merchant    model.Merchant
	outlet      model.Outlet
	transaction model.Transaction
)

// DataSeeds data to seeds
func DataSeeds() []interface{} {
	return []interface{}{
		user.Seed(),
		merchant.Seed(),
		outlet.Seed(),
		transaction.Seed(),
	}
}
