package migrations

import "api/app/model"

// ModelMigrations models to migrate
var ModelMigrations []interface{} = []interface{}{
	&model.User{},
	&model.Merchant{},
	&model.Outlet{},
	&model.Transaction{},
}
